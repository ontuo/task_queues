#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "Queue.h"

const char help_text [] = "Usage: queues [OPTION]... \r\n-f - name of file to read\r\n"
    "-p <value> - position of the element\r\n"
    "-r - read element, works with -n and -p\r\n"
    "-w <value> - write element, -n is nessesary, -p is optional (if not setted, write to the end)\r\n"
    "-n <value> - index of queue\r\n"
    "-i - information about queue, -n is nessesary\r\n"
    "-d - print queue, -n is nessesary\r\n"
    "-c concatinate queues\r\n"
    "-o <value>  print elements with 1 in bit set by args (val - bit position), -n is nessesary.\r\n";

//List of commands
typedef enum Commands
{
    READ,       // read element
    WRITE,      // write element
    INFO,       // get information about queue (size and elements)
    PRINT,      // print queue
    CONCAT,     // concatinate two queues
    PRINT_BITS, // print elements with 1 in bit set by args
    HELP        // print help
} Commands;

//Structure stores parameters set by arguments
typedef struct Params
{
    int index;          // index of queue (first or second)
    int pos;            // position of the element
    char filename[100]; // name of file to read queues
    Commands comand;    // command to process
    int write_val;      // value that should be written
    int bit_index;      // index of bit that shuold be checked (0 or 1)
} Params;

//Gets values from file and puts it to queues
int read_queues(Queue *queue1, Queue *queue2, Params *p)
{
    FILE *file;
    if (p->filename[0] == '\0') return 1;
    file = fopen(p->filename, "r");
    int string_num = 0, i = 0;
    char arr[100];
    char *ptr;
    while ((arr[i] = fgetc(file)) != EOF)
    {
        //'_' divides queues
        if (arr[i] == '_')
        {
            memset(arr, 0, sizeof(arr));
            i = 0;
            string_num++;
        }
        //'.' divides values in one queueu
        if (arr[i] == '.')
        {
            arr[i] = '\0';
            unsigned int num = strtoul(arr, &ptr, 10);
            i = 0;
            if (string_num == 0)
            {
                Queue_push(queue1, num);
            }
            if (string_num == 1)
            {
                Queue_push(queue2, num);
            }
            memset(arr, 0, sizeof(arr));
        }
        else
            i++;
    }
    fclose(file);
    return 0;
}

//Finds numbers where the specified bit is 1
void check_bits(Queue *queue, int pos)
{
    int flag = 0;
    printf("Elements with bit 1 on position %d:\n", pos);
    Node *temp;
    while (!Qeueu_is_empty(queue))
    {
        temp = Queue_pop(queue);
        //A mask is applied to a number
        if (temp->data & (1 << pos))
        {
            printf("%d ", temp->data);
            flag = 1;
        }
    }
    if (!flag)
        printf("None");
    printf("\n");
}

void print_queue(Queue *queue)
{
    Node *temp;
    while (!Qeueu_is_empty(queue))
    {
        temp = Queue_pop(queue);
        printf("%d ", temp->data);
    }
    printf("\n");
}

Queue *concatiate_queues(Queue *queue1, Queue *queue2)
{
    Queue *new_queue = (Queue *)malloc(sizeof(Queue));
    Queue_init(new_queue);
    Node *temp;
    //Queueus lenth is fixed, so we can check only one queue
    while (!Qeueu_is_empty(queue1))
    {
        temp = Queue_pop(queue1);
        Queue_push(new_queue, temp->data);
        temp = Queue_pop(queue2);
        Queue_push(new_queue, temp->data);
    }
    return new_queue;
}

//Insert new value by the given index. This function creates new queue with inserted value
Queue *insert_by_index(Queue *queue, int pos, unsigned int val)
{
    Queue *new_queue = (Queue *)malloc(sizeof(Queue));
    Queue_init(new_queue);
    int i = 0;
    Node *temp;
    while (!Qeueu_is_empty(queue))
    {
        if (i == pos)
        {
            Queue_push(new_queue, val);
            i++;
        }
        temp = Queue_pop(queue);
        Queue_push(new_queue, temp->data);
        i++;
    }
    print_queue(new_queue);
    return new_queue;
}

//Finds element by the index
void print_element(Queue *queue, int pos)
{
    Node *temp;
    int i = 0;
    while (!Qeueu_is_empty(queue))
    {
        temp = Queue_pop(queue);
        if (i == pos)
        {
            printf("Element № %d : %u\n", pos, temp->data);
            break;
        }
        i++;
    }
}

//Parse arguments and fill parameters structure
void fill_settings(Params *programm_settings, int argc, char *argv[])
{
    int argument;
    while ((argument = getopt(argc, argv, "f:p:n:rw:idco:h")) != -1)
    {
        switch (argument)
        {
        case 'p':
            //set position
            programm_settings->pos = atoi(optarg);
            break;
        case 'n':
            //set index of queue
            programm_settings->index = atoi(optarg);
            break;
        case 'f':;
            //set name of file to read
            int i = 0;
            while (optarg[i] != '\0')
            {
                programm_settings->filename[i] = optarg[i];
                i++;
            }
            break;
        case 'r':
            programm_settings->comand = READ;
            break;
        case 'w':
            programm_settings->comand = WRITE;
            programm_settings->write_val = atoi(optarg);
            break;
        case 'i':
            programm_settings->comand = INFO;
            break;
        case 'd':
            programm_settings->comand = PRINT;
            break;
        case 'c':
            programm_settings->comand = CONCAT;
            break;
        case 'o':;
            programm_settings->comand = PRINT_BITS;
            programm_settings->bit_index = atoi(optarg);
            break;
        case 'h':
            programm_settings->comand = HELP;
            break;
        }
    }
}

int main(int argc, char *argv[])
{
    //Init structure with programm settings
    Params programm_settings = {-1, -1, {0}, READ, -1, -1};
    fill_settings(&programm_settings, argc, argv);

    //Init two queues
    Queue *first_queue = (Queue *)malloc(sizeof(Queue));
    Queue *second_queue = (Queue *)malloc(sizeof(Queue));
    Queue_init(first_queue);
    Queue_init(second_queue);

    if (read_queues(first_queue, second_queue, &programm_settings) && (programm_settings.comand != HELP))
        return 1;


    switch (programm_settings.comand)
    {
    case READ:
        //read element of queue
        if (programm_settings.pos > -1)
        {
            if (programm_settings.index == 0)
                print_element(first_queue, programm_settings.pos);
            else if (programm_settings.index == 1)
                print_element(second_queue, programm_settings.pos);
            else
            {
                printf("Wrong queue index.\n");
            }
        }
        else
        {
            printf("Wrong element position.\n");
        }
        break;
    case WRITE:
        //write to queue
        if (programm_settings.index == 0)
        {
            if (programm_settings.pos == -1)
            {
                Queue_push(first_queue, programm_settings.write_val);
                printf("Inserted queue:    ");
                print_queue(first_queue);
            }
            if (programm_settings.pos <= first_queue->size)
            {
                printf("Inserted queue:    ");
                print_queue(insert_by_index(first_queue, programm_settings.pos, programm_settings.write_val));
            }
            else
                printf("Wrong element position.\n");
        }
        else if (programm_settings.index == 1)
        {
            if (programm_settings.pos == -1)
            {
                Queue_push(second_queue, programm_settings.write_val);
                printf("Inserted queue:    ");
                print_queue(second_queue);
            }
            if (programm_settings.pos <= second_queue->size)
            {
                printf("Inserted queue:    ");
                insert_by_index(second_queue, programm_settings.pos, programm_settings.write_val);
            }
            else
                printf("Wrong element position.\n");
        }
        else
            printf("Wrong queue index.\n");

        break;
    case INFO:
        //get information about queue
        if (programm_settings.index == 0)
        {
            printf("Size:  %d\n", first_queue->size);
            printf("Content:  ");
            print_queue(first_queue);
        }
        else if (programm_settings.index == 1)
        {
            printf("Size:  %d\n", second_queue->size);
            printf("Content:  ");
            print_queue(second_queue);
        }
        else
        {
            printf("Wrong queue index.\n");
        }
        break;
    case PRINT:
        //print queue
        if (programm_settings.index == 0)
        {
            printf("Content:  ");
            print_queue(first_queue);
        }
        else if (programm_settings.index == 1)
        {
            printf("Content:  ");
            print_queue(second_queue);
        }
        else
        {
            printf("Wrong queue index.\n");
        }
        break;
    case CONCAT:;
        //concatenate queues
        Queue *concat_queue;
        concat_queue = concatiate_queues(first_queue, second_queue);
        print_queue(concat_queue);
        break;
    case PRINT_BITS:;
        //print elements with 1 in bit set by args
        if (programm_settings.index == 0)
            check_bits(first_queue, programm_settings.bit_index);
        else if (programm_settings.index == 1)
            check_bits(second_queue, programm_settings.bit_index);
        else
            printf("Wrong queue index.\n");
        break;
    case HELP:
        //print help
        printf(help_text);
        break;
    default:
        printf("Wrong arguments");
        break;
    }
    return 0;
}