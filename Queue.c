#include "Queue.h"

//Initialize new empty queue
void Queue_init(Queue *queue)
{
    queue->first = NULL;
    queue->last = NULL;
    queue->size = 0;
}

//Check if queue empry
int Qeueu_is_empty(Queue *queue)
{
    if (queue == NULL)
    {
        return 1;
    }
    if (queue->size == 0)
    {
        return 1;
    }
    else
        return 0;
}

//Get queue element from the head
Node *Queue_pop(Queue *queue)
{
    Node *elem;
    if (Qeueu_is_empty(queue))
        return NULL;
    elem = queue->first;
    queue->first = (queue->first)->next;
    queue->size --;
    return elem;
}

//Add element to queue tail
int Queue_push(Queue *queue, unsigned int val)
{
    if (queue == NULL)
        return 0;
    Node *elem = (Node *)malloc(sizeof(Node));
    elem->data = val;
    elem->next = NULL;
    if (queue->size == 0)
    {
        queue->first = elem;
        queue->last = elem;
    }
    else
    {
        queue->last->next = elem;
        queue->last = elem;
    }
    queue->size++;
    return 1;
}

//Delete and free memeory
void Queue_delete(Queue *queue)
{
    Node *temp;
    while (!Qeueu_is_empty(queue))
    {
        temp = Queue_pop(queue);
        free(temp);
    }
    free(queue);
}

