#ifndef QUEUE_H
#define QUEUE_H

#include <stdio.h>
#include <stdlib.h>

//Foundation of queue - single linked list node
typedef struct Node
{
    unsigned int data;
    struct Node *next;
} Node;

//Queue has pointers to head and tail and information about number of elements
typedef struct Queue
{
    Node *first;
    Node *last;
    int size;
} Queue;


void Queue_init(Queue *queue);
int Qeueu_is_empty(Queue *queue);
Node *Queue_pop(Queue *queue);
int Queue_push(Queue *queue, unsigned int val);
void Queue_delete(Queue *queue);

#endif // !QUEUE_H
